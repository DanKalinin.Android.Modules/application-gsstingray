package application.gsstingray;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import library.gsstingray.StgBrowser;
import library.gsstingray.StgChannel;
import library.gsstingray.StgClient;
import library.gsstingray.StgEndpoint;

public class MainActivity extends AppCompatActivity implements StgBrowser.Listener {
    StgBrowser browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        browser = StgBrowser.browser(this, "_stingray-remote._tcp");
        browser.listeners.add(this);
        browser.start();
    }

    @Override
    public void endpointFound(StgBrowser sender, StgEndpoint endpoint) {
        sender.stop();

        StgClient client = StgClient.client(endpoint, "stingray");
        powerSet(client);
//        channelsGet(client);
//        channelSet(client);
    }

    @Override
    public void endpointRemoved(StgBrowser sender, String name) {
        Toast.makeText(this, "endpointRemoved", Toast.LENGTH_SHORT).show();
    }

    public void powerSet(StgClient client) {
        client.powerSetAsync(true, (exception) -> {
            if (exception == null) {
                Toast.makeText(this, "Powered on", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void channelsGet(StgClient client) {
        client.channelsGetAsync((channels, exception) -> {
            if (channels == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, channels.length + " channels available", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void channelSet(StgClient client) {
        StgChannel channel = new StgChannel();
        channel.channelListId = "TV";
        channel.channelNumber = 2;

        client.channelSetAsync(channel, (exception) -> {
            if (exception == null) {
                Toast.makeText(this, "Channel changed to - " + channel.channelNumber, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
